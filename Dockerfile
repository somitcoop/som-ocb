FROM alpine/git:latest
WORKDIR /src-odoo
ARG version
ENV odoo_version $version
RUN git clone -b $odoo_version --single-branch --depth 1 https://github.com/OCA/OCB.git odoo

FROM python:3.11-slim-bullseye

RUN useradd -ms /bin/bash odoo


COPY --from=0 /src-odoo/odoo /opt/odoo

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

# update and upgrade
RUN apt update && apt upgrade -y

# install dependencies that odoo needs to work
RUN apt install build-essential wget -y

RUN apt install libxslt-dev libzip-dev  -y

RUN apt  install libldap2-dev libsasl2-dev  -y

RUN apt install libjpeg-dev   -y

RUN apt install libpq-dev -y
RUN apt install libxml2-dev -y
RUN apt install libxslt1-dev libldap2-dev  -y
RUN apt install libsasl2-dev libffi-dev -y
# To use envsubst
RUN apt install gettext-base -y

# install node  https://packages.debian.org/bullseye/arm64/nodejs
# npm https://packages.debian.org/bullseye/arm64/npm
RUN apt install nodejs  -y
RUN apt install npm  -y
RUN apt install postgresql postgresql-client -y
# PDF
RUN apt install xfonts-75dpi wkhtmltopdf -y
# install python dependencies

RUN chown -R odoo:odoo /opt/odoo/

USER odoo

RUN pip install --upgrade pip
RUN pip install wheel
RUN pip install -r /opt/odoo/requirements.txt
RUN pip install -e /opt/odoo/
RUN pip install docutils html2text


ENV ODOO_RC /etc/odoo/odoo.conf

COPY /odoo-config/entrypoint.sh /opt/odoo/entrypoint.sh
COPY /odoo-config/wait-for-psql.py /usr/local/bin/wait-for-psql.py

USER root
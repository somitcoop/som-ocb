# som-ocb

This repository contains the Dockerfile for the som-ocb image. This image is used to have an odoo base image with the dependencies needed to run the an odoo project.


## Getting started


### Generate a production image

To generate a production image you need to run the pipeline in the version branch you want to build. The pipeline will generate an image with the name

```bash

registry.gitlab.com/somitcoop/som-ocb:$DEPLOY_VERSION_$BUILD_DAY 

```

where $DEPLOY_VERSION is the version of the image you want to build and $BUILD_DAY is the day the image was built.

### Prerequisites

* Docker

To install docker follow the instructions in the [docker documentation](https://docs.docker.com/engine/install/debian/). 
Is recommended to install the latest stable version of docker.

Or

* Podman

To install podman follow the instructions in the [podman documentation](https://podman.io/getting-started/installation).



### Login to Docker

```bash

 docker login registry.gitlab.com
    
```

## Pull the image

```bash

 docker pull registry.gitlab.com/somitcoop/som-ocb:$DEPLOY_VERSION
    
```

Where $DEPLOY_VERSION is the version of the image you want to pull. We use docker tags to version the images.

## Build the image

```bash

 docker build -t registry.gitlab.com/somitcoop/som-ocb:$DEPLOY_VERSION .
    
```

It will save an image with the name registry.gitlab.com/ocb/som-ocb:$DEPLOY_VERSION on your local docker repository.

## Push the image

```bash

 docker push registry.gitlab.com/somitcoop/som-ocb:$DEPLOY_VERSION
    
```

If you build the image with the name registry.gitlab.com/ocb/som-ocb:$DEPLOY_VERSION you can push it to the registry with the same name.

## Versioning

We use docker tags to version the images. The tags are the same as the odoo version used in the image and also have the same name as the branch used to build the image.

An example of the tags used to version the images is:
- 16-0
- 13-0

etc.